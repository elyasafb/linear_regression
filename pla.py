import random
import numpy as np
import matplotlib.pyplot as plt


class PLA(object):
    def __init__(self, positive_example, negative_example):
        self.positive_example = positive_example
        self.negative_example = negative_example
        self.w = np.array([random.uniform(-10, 10) for i in range(3)])

    def train(self):
        while not self.convergence():
            random_point, label = self.random_point()
            if label == 'positive' and self.dot_product(self.w, random_point) < 0:
                self.w = self.w + random_point
                pass
            elif label == 'negative' and self.dot_product(self.w, random_point) >= 0:
                self.w = self.w - random_point

    def convergence(self):
        for x in self.positive_example:
            if self.dot_product(self.w, x) < 0:
                return False
        for x in self.negative_example:
            if self.dot_product(self.w, x) >= 0:
                return False
        return True

    def random_point(self):
        labels = ['positive', 'negative']
        label = labels[random.randint(0, 1)]

        if label == 'positive':
            x = self.positive_example[random.randint(0, len(self.positive_example) - 1)]
        else:
            x = self.negative_example[random.randint(0, len(self.negative_example) - 1)]
        return x, label

    @staticmethod
    def dot_product(w, random_point):
        return np.matmul(w, random_point)

    def print_data(self, positive_example=None, negative_example=None):
        negative_example = negative_example if negative_example else self.negative_example
        positive_example = positive_example if positive_example else self.positive_example
        x_s, y_s = [], []
        for _, x, y in positive_example:
            x_s.append(x)
            y_s.append(y)
        x_s = np.array(x_s)
        y_s = np.array(y_s)
        plt.plot(x_s, y_s, 'r--')
        x_s, y_s = [], []
        for _, x, y in negative_example:
            x_s.append(x)
            y_s.append(y)
        x_s = np.array(x_s)
        y_s = np.array(y_s)
        plt.plot(x_s, y_s, 'bs')
        x = np.linspace(0, 100, 100)
        b, a = float(self.w[0]) / -self.w[2], float(self.w[1]) / - self.w[2]
        plt.plot(x, a * x + b, 'g^')
        plt.show()

    def test(self, test_example):
        positive_example = []
        negative_example = []
        for x in test_example:
            if self.dot_product(self.w, x) > 0:
                positive_example.append(x)
            else:
                negative_example.append(x)
        self.print_data(positive_example, negative_example)


if __name__ == '__main__':
    negative_example = [np.array([1, random.uniform(52, 100), random.uniform(52, 100)]) for i in range(50)]
    positive_example = [np.array([1, random.uniform(0, 48), random.uniform(0, 48)]) for i in range(50)]
    pla = PLA(positive_example, negative_example)
    pla.train()
    test_example = [np.array([1, random.uniform(0, 100), random.uniform(0, 100)]) for i in range(100)]
    pla.print_data()
    pla.test(test_example)
